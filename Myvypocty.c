#include "MyVypocty.h"
/* Tadeas Bartosek,  I2B
 * 27.5.2017
 * string.txt
 * hlavni cast programu
 */
long int Suma(int pole[],int pocet, int delitel) {
	int i;
	long int suma=0;
	for(i=0; i<pocet; i++) {
		if((pole[i]%delitel)==0)
			suma+=pole[i];
	}
	return suma;
}
int Suma2(int dol, int hor) {
	int sum=0;
	/*
	testovani pro kontrolni vysledek
	do{
		sum+=dol++;

	}while(dol<=hor);
	return sum;*/
	if ( dol >= hor )
		return dol;
	else {
		sum=dol;
		return (Suma2(dol+1,hor) + sum );
	}

}
double Faktorial(int cis) {
	double vys=1;
	int i;
	if(cis==0)
		return 1;

	for(i=1; i<=cis; i++) {
		vys*=i;
	}
	return vys;
}
double Rada(int n) {
	int i;
	double vys;
	for(i=0; i<=n; i++) {
		if((i%2)==0)
			vys+=(double)1/Faktorial(i);
		if((i%2)!=0)
			vys-=(double)1/Faktorial(i);
	}
	return vys;
}

