/* Tadeas Bartosek,  I2B
 * 27.5.2017
 * string.txt
 * hlavni cast programu
 */
#include "Mystring.h"
#include <stdio.h>
#include <malloc.h>
//delka stringu
int MyStrLen(char *str) {
	unsigned int i=0;
	while(str[i]!='\0') {
		i++;
	}

	return i;
}
//prevraceni retezce
void MyStrRev(char *str) {

	int i=0, e, pocet;
	char *prevracene;

	while(str[i]!='\0') {
		i++;
	}

	pocet = i;

	prevracene = (char*)malloc(sizeof(char)*pocet);

	for(e=0; e<=pocet; e++) {
		prevracene[e] = str[i];
		i--;
	}
	e=0;
	for(i=1; i<=pocet; i++) {
		str[e]=prevracene[i];
		e++;
	}
	str[e]='\0';
	free(prevracene);
	prevracene=NULL;

}

//zmenseni vsech pismen na mala
void MyDownCase(char *str) {
	unsigned int i=0;
	for(i=0; str[i]!='\0'; i++) {
		if(str[i]>='A' && str[i]<='Z')
			str[i]+=32;
	}
}
//nalezeni ynaku v retezci
char *MyStrChr(char *str,char zn) {
	//fixme
	char *found;
	unsigned int i=0;
	while(str[i]!='\0') {
		if(str[i]==zn)
			found=&str[i];
		return found;
		i++;
	}
	return NULL;

}
// porovnani reteyvu formou compareTo
int MyStrCmp(char * str1, char * str2) {
	unsigned int i=0;
	while(str1[i]!='\0') {
		if(str1[i]>str2[i]) {
			return -1;
		}
		if(str1[i]<str2[i]) {
			return 1;
		}
		i++;
	}
	return 0;
}
// kopirovani jednoho retezce do druheho s premazanim str1 prepise str2
void MyStrCpy(char *str1, char *str2) {
	unsigned int i=0;
	while(str1[i]!='\0')  {
		str2[i]=str1[i];
		i++;
	}
}
// kopirovani jednoho retezce na konec druheho

void MyStrCat(char *str1, char *str2) {
	unsigned int i=0,k;
	while(str1[i]!='\0') {

		i++;
	}
	for(k=0; str2[k]!='\0'; k++) {
		str1[(i+k)]=str2[k];
	}
}

